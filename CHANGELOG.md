# YouTube Theater Changelog

## v1.2.1 ... (25 MAY 19)

### Fixes:
`+` Fixed Watched badge so now it works on every page instead of just some.

## v1.2.0 ... (28 FEBRUARY 19)

### Changes:
`+` Reverted `Rewrote theme into LESS style.`

`+` Changed how I build the style.

## v1.1.0 ... (30 DECEMBER 18)

### Changes:
`+`Rewrote theme into LESS style.

## v1.0.5 ... (29 JULY 18)

### Fixes:
`+` Fixed top-bar when hovering while on video page

## v1.0.4 ... (26 JULY 18)

### Fixes:
`+` Fixed Real Theater Mode broken by youtube update

## v1.0.3 ... (23 JULY 18)

### Additions:
`+`Added an option to move the `Next-Bar` to the right side of the video player.

### Changes:
`+` Made scrollbar thinner so video player has a bit more space to take up.

### Notes:
`*` `Next-Bar` to the right side of the video player option will kinda fuck with video right side controls if enabled or disabled while on video page.

## v1.0.2 ... (11 JUNE 18)

### Changes:
`+` Revert Recent preprocessor change

## v1.0.1 ... (11 JUNE 18)

### Changes:
`+` Changed preprocessor for usercss

## v1.0.0 ... (22 MARCH 18)

### Initial Release
