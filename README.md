# YouTube Theater [![Version][version]][1] [![Size][size]][1] [![Install directly from GitHub][install]][2] [![GitHub stars][stars]][3] [![GitHub watchers][watchers]][4] [![GitHub open issues][open issues]][5] [![GitHub closed issues][closed issues]][5] [![GitHub license][license]][6] [![GitHub last commit][last commit]][7] [![devDependencies][devdependencies]][8]

**_Expands videos to fill all free space inside browser window_**

## Menu

- [Installation]
- [Contributions]

# Installation

1. Download one of these add-ons for your browser:
   - Stylus: [Chrome][stychrome], [Firefox][styfirefox] or [Opera][styopera].
   - xStyle: [Chrome][xstychrome], [Firefox][xstyfirefox].
2. To install directly from GitHub, click here:

   [![Install directly from GitHub][YouTube Theater]][2]

3. Done! From now on it will automatically update.

## Contributions

If you would like to contribute to this repository, please...

1. 👓 Read the [contribution guidelines][contributing].
1. ![repo-forked][9] [Fork][10] or ![cloud-download][11] [download][12] this repository.
1. 👌 Create a pull request!

<!-- BADGES -->
[version]: https://flat.badgen.net/github/release/StylusThemes/YouTube-Theater
[1]: #
[size]: https://flat.badgen.net/badgesize/normal/StylusThemes/YouTube-Theater/master/style.user.css
[install]: https://flat.badgen.net/badge/install%20directly%20from/GitHub/00ADAD "Click here!"
[2]: https://rebrand.ly/InstallYouTube-Theater
[stars]: https://flat.badgen.net/github/stars/StylusThemes/YouTube-Theater
[3]: https://github.com/StylusThemes/YouTube-Theater/stargazers
[watchers]: https://flat.badgen.net/github/watchers/StylusThemes/YouTube-Theater
[4]: https://github.com/StylusThemes/YouTube-Theater/watchers
[open issues]: https://flat.badgen.net/github/open-issues/StylusThemes/YouTube-Theater
[closed issues]: https://flat.badgen.net/github/closed-issues/StylusThemes/YouTube-Theater
[5]: https://github.com/StylusThemes/YouTube-Theater/issues
[license]: https://flat.badgen.net/github/license/StylusThemes/YouTube-Theater
[6]: https://creativecommons.org/licenses/by-sa/4.0/
[last commit]: https://flat.badgen.net/github/last-commit/StylusThemes/YouTube-Theater
[7]: https://github.com/StylusThemes/YouTube-Theater/commits/master
[devdependencies]: https://flat.badgen.net/david/dev/StylusThemes/YouTube-Theater
[8]: https://david-dm.org/StylusThemes/YouTube-Theater?type=dev
[badges]: https://flat.badgen.net/badge/amount%20of%20badges/12/orange

<!-- MENU -->
[Installation]: README.md#installation
[Contributions]: README.md#Contributions

<!-- CONTRIBUTIONS -->
[contributing]: ./.github/CONTRIBUTING.md
[9]: https://user-images.githubusercontent.com/136959/42383736-c4cb0db8-80fd-11e8-91ca-12bae108bccc.png
[10]: https://github.com/StylusThemes/YouTube-Theater/fork
[11]: https://user-images.githubusercontent.com/136959/42401932-9ee9cae0-813d-11e8-8691-16e29a85d3b9.png
[12]: https://github.com/StylusThemes/YouTube-Theater/releases

<!-- STYLUS DOWNLOADS -->
[STYChrome]: https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne
[STYFirefox]: https://addons.mozilla.org/firefox/addon/styl-us/
[STYOpera]: https://addons.opera.com/extensions/details/stylus/

<!-- XSTYLE DOWNLOADS -->
[XSTYChrome]: https://chrome.google.com/webstore/detail/xstyle/hncgkmhphmncjohllpoleelnibpmccpj
[XSTYFirefox]: https://addons.mozilla.org/firefox/addon/xstyle/

<!-- INSTALL YouTube Theater BADGE -->
[YouTube Theater]: https://flat.badgen.net/badge/YouTube%20Theater/install/00ADAD "Click here!"
